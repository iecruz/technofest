from flask_wtf import FlaskForm
from wtforms import PasswordField
from wtforms.validators import Required

class LoginForm(FlaskForm):
    password = PasswordField('Password', [Required()])
