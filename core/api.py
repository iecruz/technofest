from flask import Blueprint, request, jsonify, session, g

from core.models import Student, Attendance, Question, QuestionVote, Record, DoesNotExist, fn, JOIN

from datetime import datetime, date, time, timedelta

from random import randint

import requests

import barcode

bp = Blueprint('api', __name__)

morning = datetime.combine(date.today(), time(hour=0))
afternoon = datetime.combine(date.today(), time(hour=12))
next_day = datetime.combine(date.today() + timedelta(days=1), time(hour=0))

@bp.route('/ask_question', methods=['POST'])
def ask_question():
    if request.form.get('question') is None:
        return jsonify({'status': False}), 400
    Question.insert(
        question = request.form.get('question').capitalize(),
        sender = 'Anonymous',
        receiver = request.form.get('receiver')
    ).execute()
    return jsonify({'status': True}), 200

@bp.route('/vote_question', methods=['POST'])
def vote_question():
    if hasattr(g, 'user'):
        voter_id = g.user.get('id')
    else:
        if 'guest_voter' not in session:
            session['guest_voter'] = randint(1000, 99999)
        voter_id = session['guest_voter']

    try:
        if QuestionVote.get((QuestionVote.user_id == voter_id) & (QuestionVote.question_id == request.form.get('id'))):
            return jsonify({'status': False, 'error': 'You have already voted this question'}), 200
    except DoesNotExist:
        pass
    try:
        QuestionVote.insert(
            question_id = request.form.get('id'),
            user_id = voter_id
        ).execute()

        return jsonify({
            'status': True, 
            'count': (QuestionVote.select(fn.Count(QuestionVote.id).alias('vote_count'))
            .where(QuestionVote.question_id == request.form.get('id'))
            .get()
            .vote_count)
        }), 200
    except DoesNotExist:
        return jsonify({'status': False}), 400
    
@bp.route('/get_questions')
def get_questions():
    questions = [row for row in 
        Question.select(Question, fn.Count(QuestionVote.question_id).alias('vote_count'))
        .join(QuestionVote, JOIN.LEFT_OUTER)
        .where(Question.status==False)
        .group_by(Question)
        .order_by(fn.Count(QuestionVote.id))
        .dicts()]
    return jsonify(questions), 200

@bp.route('/get_barcode')
def get_barcode():
    barcode_id = "{}".format(g.user['id']).zfill(12)
    img = barcode.get('ean13', barcode_id)
    render_img = bytes.decode(img.render())
    return jsonify(render_img), 200

@bp.route('/answer_question', methods=['POST'])
def answer_question():
    questions = Question.update(status=True).where(Question.id == request.form.get('id')).execute()
    return jsonify(), 200

@bp.route('/scan_barcode', methods=['POST'])
def scan_barcode():
    barcode_id = request.form.get('barcode')[:-1] or request.form.get('manual')

    barcode = {}
    barcode['id'] = int(barcode_id)
    barcode['day'] = request.form.get('day')
    barcode['type'] = request.form.get('type')

    payload = {'id': barcode['id']}
    user = requests.get('http://feutech.edu.ph/scc/api/tf/get_participant', params=payload).json().get('data')

    if not user:
        return jsonify(), 400
        
    data = {}
    data['barcode'] = barcode
    data['user'] = user

    if not Record.select().where((Record.participant_id==barcode['id']) & (Record.day==barcode['day']) & (Record.record_type==barcode['type'])).execute():
        data['response'] = Record.insert(participant_id=barcode['id'], day=barcode['day'], record_type=barcode['type']).execute()
    else:
        data['response'] = False

    return jsonify(data), 200

@bp.route('/attendance', methods=['POST'])
def attendance():
    data = {}
    attendance = (Attendance.select()
        .where((Attendance.student_id==request.form.get('id')))
        .order_by(Attendance.time_in.desc())
        .dicts())
    if attendance:
        data['response'] = False
    else:
        Attendance.insert(student_id=request.form.get('id'), time_in=datetime.now()).execute()
        data['response'] = True
    return jsonify(data), 200


@bp.route('/get_attendance', methods=['GET'])
def get_attendance():
    data = {}
    data['data'] = [row for row in Student.select(Student.first_name, Student.last_name, Student.course, Attendance.time_in).join(Attendance).order_by(Student.last_name).dicts()]
    return jsonify(data), 200

@bp.route('/get_delegate', methods=['GET'])
def get_delegate():
    delegates = requests.get('http://feutech.edu.ph/scc/api/tf/get_all_participants').json()['data']
    attendance = [row for row in Record.select().where(Record.record_type=='attendance').dicts()]

    print(attendance)

    data = {}
    data['data'] = []
    for i in attendance:
        for x in delegates:
            if "{}".format(x['id']) == "{}".format(i['participant_id']):
                i['first_name'] = x['first_name']
                i['last_name'] = x['last_name']
                i['type'] = x['type']
                i['school'] = x['school']
                data['data'].append(i)
                break
        
    return jsonify(data), 200