from flask import redirect, url_for, session

from functools import wraps

def user(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in session:
            return redirect(url_for('users.login'))
        return f(*args, **kwargs)
    return decorated_function

def community(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'community_master' not in session:
            return redirect(url_for('community.login'))
        return f(*args, **kwargs)
    return decorated_function

def admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'admin' not in session:
            return redirect(url_for('admin.login'))
        return f(*args, **kwargs)
    return decorated_function

def guest(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' in session:
            return redirect(url_for('site.index'))
        return f(*args, **kwargs)
    return decorated_function
