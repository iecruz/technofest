from peewee import *
from playhouse.db_url import connect

from datetime import datetime

import os

db = connect(os.getenv('DATABASE_URL', 'postgres://postgres:postgres@localhost:5432/technofest'))

class BaseModel(Model):
    class Meta:
        database = db

class Student(BaseModel):
    id = PrimaryKeyField(index=True)
    first_name = CharField()
    last_name = CharField()
    middle_initial = CharField(null=True)
    course = CharField()
    year_level = IntegerField()
    has_voted = BooleanField(default=False)

    class Meta:
        table_name = 'students'

class Attendance(BaseModel):
    id = PrimaryKeyField(index=True)
    student_id = ForeignKeyField(Student, on_delete='cascade', on_update='cascade')
    time_in = DateTimeField()
    time_out = DateTimeField(null=True)

    class Meta:
        table_name = 'attendances'

class Record(BaseModel):
    id = PrimaryKeyField(index=True)
    participant_id = IntegerField()
    day = IntegerField()
    record_type = CharField()
    created_at = DateTimeField(default=datetime.now())

    class Meta:
        table_name = 'barcode_records'

class Question(BaseModel):
    question = TextField()
    sender = CharField(null=True)
    receiver = CharField()
    votes = IntegerField(default=0)
    status = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.now())

    class Meta:
        table_name = 'questions'

class QuestionVote(BaseModel):
    question_id = ForeignKeyField(Question)
    user_id = IntegerField()

    class  Meta:
        table_name = 'question_votes'

class Paper(BaseModel):
    id = PrimaryKeyField(index=True)
    title = CharField()
    course = CharField()
    presenter = CharField()
    mentor = CharField()

    class Meta:
        table_name = 'papers'

class PaperVote(BaseModel):
    paper_id = ForeignKeyField(Paper, on_delete='cascade', on_update='cascade')
    student_id = ForeignKeyField(Student, on_delete='cascade', on_update='cascade')

    class Meta:
        table_name = 'paper_votes'

def connect_db():
    db.connect(reuse_if_open=True)
    db.create_tables([Student, Attendance, Paper, PaperVote, Question, QuestionVote, Record], safe=True)

def close_db():
    if not db.is_closed():
        db.close()
