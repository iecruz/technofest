$(window).on('load', function() {
    $('#load').fadeOut()
});

$(window).on('load resize', function() {
    resizeFullPage();
});


function resizeFullPage() {
    if($(window).height() > $('.full-page').height()) {
        $('.full-page').height($(window).height());
    }
}

function notif_alert(message, type = 'light') {
    $('#notifContainer').slideUp('slow');
    $('#notifContainer').prepend(`<div class="alert alert-${type}" role="alert">${message}</div>`);
    $('#notifContainer').slideDown('slow');
    
    setTimeout(function() {
        $('#notifContainer').slideUp('slow', function() {
            $('#notifContainer').html('');
        });
    }, 3000);
}

function requestFullScreen(element) {
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

    if (requestMethod) {
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") {
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}