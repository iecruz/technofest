from flask import Blueprint, render_template, redirect, session, request, url_for, flash

from core.wrappers import guest, user

import requests

bp = Blueprint('users', __name__)

@bp.route('/login', methods=['GET', 'POST'])
@guest
def login():
    error = None
    if request.method == 'POST':
        payload = {'email_address': request.form.get('email_address')}
        user_data = requests.get('http://feutech.edu.ph/scc/api/tf/get_participant', params=payload).json().get('data')
        if user_data:
            session['user'] = user_data
            flash("Welcome to 1st International TechnoFest, {}!".format(user_data.get('first_name')))
            return redirect(url_for('site.index'))
        else:
            error = 'You are not registred to TechnoFest'
    return render_template('login.html', error=error)

@bp.route('/logout', methods=['POST'])
@user
def logout():
    session.pop('user')
    return redirect(url_for('users.login'))