from flask import Blueprint, render_template, session, redirect, url_for

from core.models import Question, QuestionVote, Paper, PaperVote, fn, JOIN
from core.forms import LoginForm
from core.wrappers import admin, guest

bp = Blueprint('admin', __name__)

@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.password.data == 'weareone':
            session['admin'] = True
            return redirect(url_for('admin.index'))
        else:
            form.password.errors.append('Incorrect Password')
    return render_template('admin/login.html', form=form)

@bp.route('/logout', methods=['GET', 'POST'])
@admin
def logout():
    session.pop('admin')
    return redirect(url_for('admin.login'))

@bp.route('/')
@admin
def index():
    return render_template('admin/index.html')

@bp.route('/chat')
@admin
def chat():
    return render_template('admin/chat.html')

@bp.route('/forum')
@admin
def forum():
    questions = [row for row in 
        Question.select(Question, fn.Count(QuestionVote.question_id).alias('vote_count'))
        .join(QuestionVote, JOIN.LEFT_OUTER)
        .where(Question.status == False)
        .group_by(Question)
        .order_by(fn.Count(QuestionVote.id).desc())
        .dicts()]
    return render_template('admin/forum.html', questions=questions)

@bp.route('/announcement')
@admin
def announcement():
    return render_template('admin/announcement.html')

@bp.route('/barcode')
@admin
def barcode():
    return render_template('admin/barcode.html')

@bp.route('/attendance')
@admin
def attendance():
    return render_template('admin/attendance.html')

@bp.route('/delegate')
@admin
def delegate():
    return render_template('admin/delegate.html')

@bp.route('/vote')
@admin
def vote():
    votes = [row for row in Paper.select(Paper, fn.Count(PaperVote.id).alias('vote_count'))
        .join(PaperVote, JOIN.LEFT_OUTER)
        .group_by(Paper)
        .order_by(Paper.id)
        .dicts()]
    return render_template('admin/vote.html', votes=votes)