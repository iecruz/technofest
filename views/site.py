from flask import Blueprint, render_template, url_for, g

from core.wrappers import user

import barcode

bp = Blueprint('site', __name__)

@bp.route('/')
@user
def index():
    menu = [
        {
            'name': 'Speakers',
            'link': url_for('site.speakers'),
            'icon': 'fas fa-graduation-cap'
        },{
            'name': 'Forum',
            'link': '/forum',
            'icon': 'fas fa-exchange-alt'
        },{
            'name': 'Competitions',
            'link': url_for('site.competitions'),
            'icon': 'fas fa-trophy'
        },{
            'name': 'Message',
            'link': url_for('site.message'),
            'icon': 'fas fa-comments'
        },{
            'name': 'Barcode',
            'link': url_for('site.barcode_image'),
            'icon': 'fas fa-barcode'
        },{
            'name': 'Facebook',
            'link': 'https://facebook.com/feutechscc',
            'icon': 'fab fa-facebook'
        },{
            'name': 'Twitter',
            'link': 'https://twitter.com/feutechscc',
            'icon': 'fab fa-twitter'
        }
    ]
    return render_template('index.html', menu_icons=menu)

@bp.route('/barcode')
@user
def barcode_image():
    return render_template('barcode.html')

@bp.route('/speakers')
@user
def speakers():
    speaker = []


    speaker.append({
        'img': url_for('static', filename='img/speakers/0.jpg'),
        'name': 'Ludwig Feredigan',
        'description': 'Climate Reality Leader of The Climate Reality Project, Philippines',
        'link': url_for('site.profile', id='0')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/1.png'),
        'name': 'Rindu Alriavindrafunny',
        'description': 'Lecturer of Institute Technology Adisutjipto',
        'link': url_for('site.profile', id='1')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/2.jpg'),
        'name': 'Dr. Manuel Belino',
        'description': 'Senior Director for Engineering of FEU Institute of Technology',
        'link': url_for('site.profile', id='2')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/3.jpg'),
        'name': 'Yacine Petitprez',
        'description': 'IT Manager of Ingedata International',
        'link': url_for('site.profile', id='3')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/4.jpg'),
        'name': 'Arlene Romasanta',
        'description': 'OIC, Systems and Infrastructure Development Service (SID) of Department of Information and Communication Technology',
        'link': url_for('site.profile', id='4')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/5.jpg'),
        'name': 'Dr. Michael Choy Seng Kim',
        'description': 'Founder of Dioworks Learning',
        'link': url_for('site.profile', id='5')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/6.png'),
        'name': 'Dr. Alberto Amorsolo',
        'description': 'Professor of UP College of Engineering',
        'link': url_for('site.profile', id='6')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/7.jpg'),
        'name': 'Neil Stephen Lopez',
        'description': 'Assistant Professor, Vice Chair and External Affairs Coordinator, Mechanical Engineering of De La Salle University',
        'link': url_for('site.profile', id='7')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/8.jpg'),
        'name': 'Dr. Rosemary Vesa',
        'description': 'President of the Human Factors and Ergonomics Society of the Philippines, Philippines',
        'link': url_for('site.profile', id='8')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/9.jpg'),
        'name': 'Atty. Analiza Rebuelta-Teh',
        'description': 'Interim Undersecretary for Mining of Department of Environment and Natural Resources (DENR), Philippines',
        'link': url_for('site.profile', id='9')
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/10.jpg'),
        'name': 'Virgilio Leyba',
        'description': 'Information and Technology Services Department Manager of National Power Corporation, Philippines',
        'link': url_for('site.profile', id='10')
    })
    return render_template('speakers.html', speaker=speaker)

@bp.route('/profile/<int:id>')
@user
def profile(id):
    speaker = [];

    speaker.append({
        'img': url_for('static', filename='img/speakers/0.jpg'),
        'name': 'Ludwig Feredigan',
        'description': 'Climate Reality Leader of The Climate Reality Project, Philippines',
        'summary': [
            'Graduate of Bachelor of Science in Engineering major in Electronics & Communications (BSECE) at the Don Bosco Technical College',
            'Graduate of Masters in Business Administration at the University of San Carlos of Cebu City',
            'Strategic Business Economic Program graduate at the University of Asia & the Pacific',
            'Finished diploma in Supply Chain Management under the Business & Training Linkage Program of the PUM Entrepreneurs for Entrepreneurs at the Hague Netherlands',
            'Recipient of a number of fellowships and scholarships on executive development programs on climate change, sustainable consumption and production, green energy and climate finance, and leadership',
            'Expert Reviewers on the first order draft of the Intergovernmental Panel on Climate Change (IPCC) Special Report on Global Warming of 1.5ºC (SR15)',
            'Miguel R. Magalang Individual Climate Leadership Memorial Awardee given by The Climate Reality Project Philippines',
            'Dangal ng Kalikasan Awardee given by the Mister Earth Philippines'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/1.png'),
        'name': 'Rindu Alriavindrafunny',
        'description': 'Lecturer of Institute Technology Adisutjipto',
        'summary': [
            'Masters degree on Mathematics Education in State University of Surabaya',
            'Masters degree on Science Education Communication and Technology (SECT), Utrecht University, Utrecht Belanda',
            'Instructor of Elementary, Middle School, and Senior High School in the different schools in Indonesia',
            'Mathematics Lecturer of the Aeronatica Engineering Department and the Head of Student Affairs of Institute of Technology Adisutjpto, Yogyakarta, Indonesia',
            'Master Course in Science Education Communication and Technology (SECT), Utrecht University, Utrecht Belanda'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/2.jpg'),
        'name': 'Dr. Manuel Belino',
        'description': 'Senior Director for Engineering of FEU Institute of Technology',
        'summary': [
            'Bachelor’s degree in Mechanical Engineering, Mapua Institute of Technology, Philippines',
            'Masters degree in Engineering Education, De La Salle University. Philippines',
            'Master’s degree of Theological Studies, Havard University, USA',
            'Doctorate degree in Religious and Values Education, De La Salle University',
            'Chair of the Commission on Higher Education (CHED) Technical Committee in Mechanical Engineering',
            'Published in local and international journals in the fields of engineeting ethics, engineering education and safety engineering',
            'Featured in the Metrobank 50th anniversary publication Ten Outstanding Filipino Teachers',
            'President, Philippine Institute of Mechanical Engineering Educators',
            'Senior Director for Engineering,Far Eastern University Institute of Technology'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/3.jpg'),
        'name': 'Yacine Petitprez',
        'description': 'IT Manager of Ingedata International',
        'summary': [
            'IT Manager, Ingedata Philippines',
            'Founder and Chief Operating Officer of the Digital Bakery LTD., Philippines',
            'IT Consultant of Iscale, Philippines an Web application factory, focused on delivering custom made applications for international customers',
            'Trainer of the programming language, Ruby on Rails in France in year 2013'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/4.jpg'),
        'name': 'Arlene Romasanta',
        'description': 'OIC, Systems and Infrastructure Development Service (SID) of Department of Information and Communication Technology',
        'summary': [
            'Bachelor of Science in Computer Data Processing Management, Polytechnic University of the Philippines',
            'Master’s degree in Productivity and Quality Management with Leadership Award, Development Academy of the Philippines',
            'Took international course, “Formulating and Managing GIS Projects” at the International Institute for Geo-Information Science and Earth Observation, Enschede, The Netherlands',
            'Co-Project Lead in the International Collaborative Research on Disaster Response Model Using Vehicle Communications that was funded by Asia Pacific Telecommunity',
            'OIC Director of the Systems and Infrastructure Development Service, Department of Information and Communications Technology (DICT)',
            'Division Chief/Information Technology Officer 3, Strategic R&D Services Division (SRDSD) Systems and Infrastructure Development Service (SIDS), DICT'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/5.jpg'),
        'name': 'Dr. Michael Choy Seng Kim',
        'description': 'Founder of Dioworks Learning',
        'summary': [
            'Senior educator and researcher in the area of facilitation, learning design and development',
            'Trainer of more than 5000 adult educators and teachers across China, Indonesia, Malaysia and Singapore in the area of learning styles, design skills and pedagogy',
            'Former Assistant Director and Head, Design and Development Unit, Institute for Adult Learning, WDA',
            'Manager of curriculum and MOOC (Massive Online Open Courseware) development',
            'Founder Dioworks Learning, his own e-learning company, designing MOOCs',
            'Facilitator of enterprises to adopt e-learning as part of the workplace learning initiative',
            'Published numerous articles in academic journals and conference publications',
            'Writer in Educational Media International and Cogent Education discussed the impact of pedagogical beliefs of teachers on technology adoption in the classroom'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/6.png'),
        'name': 'Dr. Alberto Amorsolo',
        'description': 'Professor of UP College of Engineering',
        'summary': [
            'Former Member of Board of Directors and Founding Member, Institute of Materials Engineers of the Philippines',
            'Bachelor of Science in Metallurgical Engineering, Cum Laude, University of the Philippines',
            'Certified in Mineral Processing and Metallurgy awarded by the Japan International Cooperation Agency and SENKEN in Tohuku University, Sendai, Japan',
            'Acquired a Diploma in Extractive Metallurgy that was awarded by SUNKEN in the Tohuku University, Sendai, Japan',
            'Masters degree, University of the Philippines',
            'Masters degree in Material Science, University of Rochester, New York, USA',
            'Doctorate degree in Material Science, University of Rochester, New York, USA',
            'Professor, College of Engineering University of the Philppines'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/7.jpg'),
        'name': 'Neil Stephen Lopez',
        'description': 'Assistant Professor, Vice Chair and External Affairs Coordinator, Mechanical Engineering of De La Salle University',
        'summary': [
            'Bachelors degree in Mechanical Engineering, Magna Cum Laude, De La Salle University',
            'Masters degree in Mechanical Engineering',
            'Awardee of Gold Medal for Outstanding Master’s Thesis. His Master’s Thesis is entitled “Energy modeling, fabrication and evaluation of a solar dryer to lower the production cost of biofuel from micoralgae”',
            'Candidate for Doctor of Philosophy in Mechanical Engineering in the De La Salle University',
            'Visiting PhD Researcher in the different countries',
            'Assistant Professor and Vice Chair and External Affairs Coordinator of Mechanical Engineering Department of the De La Salle University, Philippines'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/8.jpg'),
        'name': 'Dr. Rosemary Vesa',
        'description': 'President of the Human Factors and Ergonomics Society of the Philippines, Philippines',
        'summary': [
            
            'Dean of the Gokongwei College of Engineering - De La Salle University',
            'Top 250 scholars and scientists in the Philippines from Webometrics in 2015 and 2016',
            'Current President of the Human Factors and Ergonomics Society of the Philippines (HFESP) and the past President of the Southeast Asian Network of Ergonomics Societies (SEANES)',
            'Doctorate degree, Nanyang Technological University (Singapore)',
            'Masters degree in Ergonomics, University of New South Wales (Australia)',
            'Masters degree in Industrial Engineering, De La Salle University',
            'Ausaid scholar and a recipient of Distinguished Educator Award from the Industrial Engineering and Operations Management (IEOM) Society',
            'First Vice President of the Philippine Association of Engineering Schools (PAES) from 2012-2015'

        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/9.jpg'),
        'name': 'Atty. Analiza Rebuelta-Teh',
        'description': 'Interim Undersecretary for Mining of Department of Environment and Natural Resources (DENR), Philippines',
        'summary': [
            'Interim Undersecretary For Mining',
            'Undersecretary, Climate Change Service Department of Environment and Natural Resources (DENR)',
            'Permanent Alternate, Mining Industry Coordinating Council (MICC)',
            'Permanent Alternate, Philippine Mining Development Corporation (PMDC)',
            'Permanent Alternate, Mines Adjudication Board (MAB)',
            'Focal Point Of The National Designated Authority, Green Climate Fund',
            'Alternate Representative To The Undersecretary For Legal Affairs, National Power Corporation (NPC)',
            'DENR Representative, National Task Force for the West Philippine Sea',
            'CHAIRPERSON, National Gender and Development Focal Point System Department of Environment and Natural Resources (DENR)',
            'Permanent Representative, National Advisory Council National Youth Commission (NYC)',
            'National Focal Point, East Asian Seas Partnership Council Partnerships in Environmental Management for the Seas of East Asia (PEMSEA)'
        ]
    })
    speaker.append({
        'img': url_for('static', filename='img/speakers/10.jpg'),
        'name': 'Virgilio Leyba',
        'description': 'Information and Technology Services Department Manager of National Power Corporation, Philippines',
        'summary': [
            'Graduate of Bachelor of Science in Computer Engineering at the Mapua University and was awarded of Pres. Oscar B. Mapua Silver Scholarship Medal',
            'Graduate of Master in Business Administration at the National College of Business and Arts',
            'Cisco Certified Network Associate',
            'Department Manager, National Power Corporation, Information and Technology Services Department'
        ]
    })

    return render_template('profile.html', speaker=speaker[id])

@bp.route('/competitions')
@user
def competitions():
    competition = []

    competition.append({
        'img': url_for('static', filename='img/competitions/1.png'),
        'description': 'Rube Goldberg is simple and compound machines are designed to make work easier. Through the cartoons of Rube Goldberg, students are engaged in critical thinking about the way his invention make simple tasks even harder to complete.'
    })

    competition.append({
        'img': url_for('static', filename='img/competitions/3.png'),
        'description': 'This event will provide seminars about Android Programming. This event will also provide programming competitions based on the Android Language that can help the participants understand more the language. Also, seminars will cover the basic up to the intermediate aspects of Android Programming. There will be prizes in store for the winners of the programming competition.'
    })

    competition.append({
        'img': url_for('static', filename='img/competitions/4.png'),
        'description': 'The event is about testing the mental and physical capabilities. The first round is a qualifying round where participants group into fives to take an individual exam to accumulate certain points to proceed to the next round where they will partake to an amazing race-like game. The second round is a race where teams go head to head in finishing certain tasks that is assigned in a station, the fastest teams to finish the race will move on to the final round. The final round is a quiz-bee competition with a twist that consists of easy, moderate and difficult rounds.'
    })

    competition.append({
        'img': url_for('static', filename='img/competitions/5.png'),
        'description': 'This tends to test the skill of every electrical engineering student to do a circuit within the given time. There are only 5 teams and each team only has 5 members. Each member is given only 1 minute to do a part of the circuit before passing it on to the next team member. This game will practice the engineering students to be calm and concentrate when they have a limited time only to do their part. The winner of this game will be judged by their creativity making a circuit and when their circuit will operate properly.'
    })

    competition.append({
        'img': url_for('static', filename='img/competitions/6.png'),
        'description': 'This fun, educational, and awesome activity will let the participating Mechanical Engineering Students build their own small-scale model of roller coasters using recyclable materials, mainly by drinking straws; ping pong balls will be used to represent the passengers of the coaster and to test the ride.'
    })

    competition.append({
        'img': url_for('static', filename='img/competitions/7.png'),
        'description': 'The competition will cater to the general Information Technology Community. It will be a system design and development competition where participants will be given one whole day to create a web application with analytics that is in line with the Technofest theme. On the second day of the competition, participants will be pitching and proposing their ideas and web systems.'
    })

    return render_template('competitions.html', competition=competition)

@bp.route('/forum')
def forum():
    speakers = [
        'Alberto V. Amorsolo Jr.',
        'Dr. Michael Choy Seng Kim',
        'Dr. Rosemary R. Seva',
        'Virgilio S. Leyba'
    ]
    return render_template('forum.html', speakers=speakers)

@bp.route('/message')
@user
def message():
    return render_template('message.html')