from flask import Blueprint, render_template, url_for, request, flash, redirect, session

from core.models import Student, Paper, PaperVote, DoesNotExist
from core.forms import LoginForm
from core.wrappers import community, guest

bp = Blueprint('community', __name__)

@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.password.data == 'technofest2018':
            session['community_master'] = True
            return redirect(url_for('community.attendance'))
        else:
            form.password.errors.append('Incorrect Password')
    return render_template('community/login.html', form=form)

@bp.route('/logout', methods=['GET', 'POST'])
@community
def logout():
    session.pop('community_master')
    return redirect(url_for('community.login'))

@bp.route('/')
@community
def index():
    return redirect(url_for('community.attendance'))

@bp.route('/attendance')
@community
def attendance():
    students = [row for row in Student.select().order_by(Student.last_name, Student.first_name).dicts()]
    return render_template('community/attendance.html', students=students)

@bp.route('/vote', methods=['GET', 'POST'])
@community
def vote():
    if request.method == 'POST':
        if not Student.select().where(Student.id==request.form.get('student_id')).get().has_voted:
            data = {}
            data = [{'student_id': request.form.get('student_id'), 'paper_id': row} for row in request.form.getlist('paper_id[]') if int(row) != 0]
            PaperVote.insert_many(data).execute()
            Student.update(has_voted=True).where(Student.id==request.form.get('student_id')).execute()
            flash('You have voted successfully.')

    students = [row for row in Student.select().where(Student.has_voted==False).order_by(Student.last_name, Student.first_name).dicts()]
    papers = [row for row in Paper.select().order_by(Paper.id).dicts()]
    programs = ['CE', 'CPEO', 'CS', 'ECE', 'EE', 'ITAGD', 'ITDA', 'ITSMBA', 'ITWMA']

    return render_template('community/vote.html', students=students, papers=papers, programs=programs)
