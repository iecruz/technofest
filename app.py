from flask import Flask, render_template, session, g, request
from flask_socketio import SocketIO, emit, join_room, leave_room, close_room, rooms
from flask_cors import CORS

from gevent import monkey

from core import models, api
from core.models import connect_db, close_db

from views import site, admin, community, users

import os

monkey.patch_all()

app = Flask(__name__)
CORS(app)
socketio = SocketIO(app)

env = os.getenv('ENVIRONMENT', default='development')
if env == 'production':
    app.config.from_object('core.config.ProductionConfig')
elif env == 'development':
    app.config.from_object('core.config.DevelopmentConfig')

app.register_blueprint(site.bp)
app.register_blueprint(admin.bp, url_prefix='/admin')
app.register_blueprint(community.bp, url_prefix='/community')
app.register_blueprint(api.bp, url_prefix='/api')
app.register_blueprint(users.bp)

admin_sid = []
chat_room = []

@app.before_request
def before_request():
    connect_db()
    if session.get('user'):
        g.user = session.get('user')
    if session.get('community_master'):
        g.community = session.get('community_master')
    if session.get('admin'):
        g.admin = session.get('admin')

@app.teardown_request
def teardown_request(exception):
    close_db()

@socketio.on('admin join', namespace='/chat')
def on_join_admin():
    admin_sid.append(request.sid)
    for i in chat_room:
        join_room(i)

@socketio.on('join', namespace='/chat')
def on_join(data):
    chat_room.append(data['id'])
    join_room(data['id'])
    for i in admin_sid:
        join_room(data['id'], sid=i)

@socketio.on('send message', namespace='/chat')
def on_send_message(data):
    emit('deliver message', data, room=data['id'], include_self=False)

if __name__ == '__main__':
    socketio.run(app, debug=app.config['DEBUG'])
